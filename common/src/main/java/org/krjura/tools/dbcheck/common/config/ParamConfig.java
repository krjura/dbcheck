package org.krjura.tools.dbcheck.common.config;

import static org.krjura.tools.dbcheck.common.config.utils.ParamHelper.ensureParam;
import static org.krjura.tools.dbcheck.common.config.utils.ParamHelper.intOrDefault;
import static org.krjura.tools.dbcheck.common.config.utils.ParamHelper.parseArgs;

import java.util.Map;

public class ParamConfig {

    private final String url;
    private final String username;
    private final String password;
    private final Integer waitTime;
    private final Integer attempts;

    private ParamConfig(String[] args) {

        Map<String, String> params = parseArgs(args);

        this.url = ensureParam("--url", params);
        this.username = ensureParam("--username", params);
        this.password = ensureParam("--password", params);
        this.waitTime = intOrDefault("--waitTime", 5000, params);
        this.attempts = intOrDefault("--count", 10, params);
    }

    public static ParamConfig of(String[] args) {
        return new ParamConfig(args);
    }

    public String getUrl() {
        return url;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public Integer getWaitTime() {
        return waitTime;
    }

    public Integer getAttempts() {
        return attempts;
    }
}
