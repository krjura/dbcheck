package org.krjura.tools.dbcheck.common.utils;

import org.krjura.tools.dbcheck.common.config.ParamConfig;

public class ThreadUtils {

    private ThreadUtils() {
        // private
    }

    public static void sleep(ParamConfig config) {
        try {
            Thread.sleep(config.getWaitTime());
        } catch (InterruptedException e) {
            // don't care
        }
    }
}
