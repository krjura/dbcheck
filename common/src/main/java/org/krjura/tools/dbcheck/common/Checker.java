package org.krjura.tools.dbcheck.common;

import org.krjura.tools.dbcheck.common.config.ParamConfig;
import org.krjura.tools.dbcheck.common.utils.Logger;
import org.krjura.tools.dbcheck.common.utils.ThreadUtils;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;
import java.util.Properties;

public class Checker {

    private final String[] args;

    private final Driver driver;

    private final String testQuery;

    private Checker(String[] args, Driver driver, String testQuery) {
        this.args = Objects.requireNonNull(args);
        this.driver = Objects.requireNonNull(driver);
        this.testQuery = Objects.requireNonNull(testQuery);
    }

    public static Checker of(String[] args, Driver driver, String testQuery) {
        return new Checker(args, driver, testQuery);
    }

    public void check()  {

        ParamConfig config = ParamConfig.of(this.args);

        Logger.info("using url: %s", config.getUrl());

        for(int i = 0; i < config.getAttempts(); i++) {
            try {
                connect(config);
                return;
            } catch (SQLException e) {
                Logger.warn("Cannot connect to database. Message was: %s", e.getMessage());
                e.printStackTrace();
            }

            ThreadUtils.sleep(config);
        }
    }

    private void connect(ParamConfig config)
            throws SQLException {

        Logger.info("using driver %s", this.driver);

        try (
                Connection connection = getConnection(config.getUrl(), config.getUsername(), config.getPassword());
                PreparedStatement pstmt = connection.prepareStatement(this.testQuery);
                ResultSet result = pstmt.executeQuery()) {

            if(result.next()) {
                Logger.info("Connection successful");
            } else {
                Logger.info("Connection failed");
            }
        }
    }

    private Connection getConnection(String url, String username, String password) throws SQLException {

        Properties properties = new Properties();
        properties.put("user", username);
        properties.put("password", password);

        return this.driver.connect(url, properties);
    }
}
