package org.krjura.tools.dbcheck.common.config.utils;

import org.krjura.tools.dbcheck.common.utils.Logger;

import java.util.HashMap;
import java.util.Map;

public class ParamHelper {

    private ParamHelper() {
        // utils
    }

    public static Map<String, String> parseArgs(String[] args) {
        Map<String, String> params = new HashMap<>(args.length);

        for(String arg : args) {
            int index = arg.indexOf('=');

            if(index == -1) {
                Logger.warn("Invalid param %s", arg);
                System.exit(1);
            }

            String key = arg.substring(0, index);
            String value = arg.substring(index+1);

            params.put(key, value);
        }

        return params;
    }

    public static Integer intOrDefault(String param, int defaultValue, Map<String, String> params) {
        String value = params.get(param);

        if(value == null) {
            return defaultValue;
        }

        return Integer.parseInt(value);
    }

    public static String ensureParam(String param, Map<String, String> params) {
        String value = params.get(param);

        if(value == null) {
            Logger.warn("missing param %s", param);
            System.exit(0);
        }

        return value;
    }
}
