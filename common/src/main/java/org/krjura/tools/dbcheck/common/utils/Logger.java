package org.krjura.tools.dbcheck.common.utils;

public class Logger {

    private Logger() {
    }

    public static void warn(String message, Object... args) {
        System.out.println("[WARN] " + String.format(message, args)) ;
    }

    public static void info(String message) {
        System.out.println("[INFO] " + message);
    }

    public static void info(String message, Object... args) {
        System.out.println("[INFO] " + String.format(message, args));
    }
}
