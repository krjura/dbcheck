package  org.krjura.tools.dbcheck.informix;

import com.informix.jdbc.IfxDriver;
import com.informix.jdbc.IfxSqliConnect;
import org.krjura.tools.dbcheck.common.Checker;
import org.krjura.tools.dbcheck.common.utils.Logger;

public class InformixCheck {

    public static void main(String[] args) {
        // init for graalvm
        IfxSqliConnect.class.getSimpleName();

        Logger.info("loading driver %s", IfxDriver.class.getCanonicalName());
        Checker.of(args, new IfxDriver(), "select count(*) from systables").check();
    }
}
