#!/bin/bash

DIR=$(pwd)
BUILD_DIR=/opt/build
NAME=postgres-check

./gradlew -p postgresql clean build installDist
docker run \
  -v $DIR:$BUILD_DIR \
  docker.krjura.org/dbcheck/build-env:1 \
  bash -c "native-image --no-fallback -H:+ReportExceptionStackTraces -H:Name=$NAME --initialize-at-build-time --allow-incomplete-classpath --class-path 'postgresql/build/install/postgresql/lib/*' org.krjura.tools.dbcheck.postgresql.PostgreSqlCheck; chown $UID $NAME"