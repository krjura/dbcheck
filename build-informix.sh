#!/bin/bash

DIR=$(pwd)
BUILD_DIR=/opt/build
NAME=informix-check

./gradlew -p informix build installDist
docker run \
  -v $DIR:$BUILD_DIR \
  docker.krjura.org/dbcheck/build-env:1 \
  bash -c "native-image --no-fallback -H:+ReportExceptionStackTraces -H:Name=$NAME --initialize-at-build-time --allow-incomplete-classpath --class-path 'informix/build/install/informix/lib/*' org.krjura.tools.dbcheck.informix.InformixCheck; chown $UID $NAME"