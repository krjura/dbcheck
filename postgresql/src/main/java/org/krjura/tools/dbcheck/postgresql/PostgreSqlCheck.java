package org.krjura.tools.dbcheck.postgresql;

import org.krjura.tools.dbcheck.common.Checker;
import org.krjura.tools.dbcheck.common.utils.Logger;
import org.postgresql.Driver;

public class PostgreSqlCheck {

    public static void main(String[] args) {
        Logger.info("loading driver %s", Driver.class.getCanonicalName());
        Checker.of(args, new Driver(), "SELECT 1").check();
    }
}